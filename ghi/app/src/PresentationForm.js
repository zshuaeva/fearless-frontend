import React, {useEffect, useState} from 'react';

function PresentationForm (props) {
    const [conferences, setConferences] = useState ([]);

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [companyName, setcompanyName] = useState ('');
    const [title, setTitle] = useState ('');
    const [synopsis, setSynopsis] = useState ('');
    const [conference, setConference] = useState ('');


    const handleConferenceChange = event => {
        const value = event.target.value
        setConference(value)
    }

    const handleNameChange = event => {
        const value = event.target.value
        setName(value)
    }

    const handleEmailChange = event => {
        const value = event.target.value
        setEmail(value)
    }

    const handleCompanyNameChange = event => {
        const value = event.target.value
        setcompanyName(value)
    }

    const handleTitleChange = event =>{
        const value = event.target.value
        setTitle(value)
    }

    const handleSynopsisChange = event =>{
        const value = event.target.value
        setSynopsis(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.presenter_name = name;
        data.presenter_email = email;
        data.conference = conference;
        data.company_name = companyName;
        data.title = title;
        data.synopsis = synopsis;
        console.log(data);

        const conferenceUrl = `http://localhost:8000${data.conference}presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
    };

    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
        const newPresentation = await response.json();
        console.log(newPresentation);

        setName('');
        setEmail('');
        setConference('');
        setcompanyName('');
        setTitle('');
        setSynopsis('');
    }
}

  const fetchData = async () => {
      const url = "http://localhost:8000/api/conferences/";

      const response = await fetch(url);

      if (response.ok) {
          const data = await response.json();
          setConferences(data.conferences)
      }
  }

  useEffect(() => {
      fetchData();
  }, []);

  return (
      <div className="row">
          <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
              <h1>Create a new presentation</h1>
              <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                  <input placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" onChange={handleNameChange} value={name} /> <label htmlFor="name">Presenter name</label>
              </div>

              <div className="form-floating mb-3">
                  <input placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control" onChange={handleEmailChange} value={email} /> <label htmlFor="name">Presenter email</label>
              </div>

              <div className="form-floating mb-3">
                  <input placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control" onChange={handleCompanyNameChange} value={companyName} /> <label htmlFor="name">Company name</label>
              </div>

              <div className="form-floating mb-3">
                  <input placeholder="Title" required type="text" name="title" id="title" className="form-control" onChange={handleTitleChange} value={title} /> <label htmlFor="name">Title</label>
              </div>

              <div className="mb-3">
                  <label htmlFor="synopsis" className="form-label">Synopsis</label> <textarea placeholder="Enter synopsis..." required className="form-control" name="synopsis" id="synopsis" ows="4" value={synopsis} onChange={handleSynopsisChange} ></textarea>
              </div>

              <div className="mb-3">
              <select name="conference" id="conference" className= 'form-select' required value={conference} onChange={handleConferenceChange} >
                  <option key="default" value="">Choose a conference</option>
                  {conferences.map(conference => {
                  return (
                      <option key={conference.href} value={conference.href}>
                      {conference.name}
                      </option>
                  );
                  })}
                  </select>
              </div>

              <button className="btn btn-primary">Create</button>
              </form>
          </div>
          </div>
      </div>
  );
  };

export default PresentationForm;
